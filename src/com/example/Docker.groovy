#!/usr/bin/env groovy

package com.example

class Docker implements Serializable{

    def script

    Docker(script) {
        this.script = script
    }

    def buildDockerImage(String imageName) {
        script.echo "building the docker image..."
        script.sh "docker build -t $imageName ."
    }

    def dockerLogin() {
        script.echo "docker login..."
        script.withCredentials([script.usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin"
        }
    }

    def dockerPush(String imageName) {
        script.echo "docker push..."
        script.sh "docker push $imageName"
    }

    def dockerRun(String imageName, String dockerContainerName, String credential, String remoteUrl) {
        def dockerStop = "docker stop $dockerContainerName"
        def dockerRm = "docker rm $dockerContainerName"
        def dockerRun = "docker run --name $dockerContainerName -d -p 3000:3000 tmoretto17/my-app:$imageName"
        script.sshagent(["${credential}"]) {
            script.sh "ssh -o StrictHostKeyChecking=no ${remoteUrl} '${dockerStop}; ${dockerRm}; ${dockerRun}; docker ps;'"
        }
    }

    def dockerCompose(String imageName, String credential, String remoteUrl) {
        def shellCmd = "bash ./server-cmds.sh ${imageName}"

        script.sshagent(["${credential}"]) {
            script.sh "scp server-cmds.sh ${remoteUrl}:/home/ec2-user"
            script.sh "scp docker-compose.yaml ${remoteUrl}:/home/ec2-user"
            script.sh "ssh -o StrictHostKeyChecking=no ${remoteUrl} ${shellCmd}"
        }
    }

}