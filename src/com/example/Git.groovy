#!/usr/bin/env groovy

package com.example

class Git implements Serializable {

    def script

    Git(script) {
        this.script = script
    }

    def setJenkinsUser() {
        script.sh 'git config --global user.email "jenkins@example.com"'
        script.sh 'git config --global user.name "jenkins"'
    }

    def gitCommitAndPush(String repo) {
        script.withCredentials([script.usernamePassword(credentialsId: 'gitlab-login', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            def user = script.USER
            def pass = script.PASS
            script.sh "git remote set-url origin https://${user}:${pass}@${repo}"
            script.sh 'git add ./app/package.json'
            script.sh 'git commit -m "version update"'
            script.sh "git push origin HEAD:$script.BRANCH_NAME"
        }
    }
}