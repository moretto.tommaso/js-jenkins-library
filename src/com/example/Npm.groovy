#!/usr/bin/env groovy

package com.example

class Npm implements Serializable{
    def script

    Npm(script) {
        this.script = script
    }

    def npmInstall() {
        script.echo "Building application..."
        script.sh 'npm install'
    }

    def testing() {
        script.echo "Testing application..."
        script.sh 'npm install mocha'
        script.sh 'npm test'
    }

    def retrievingVersion() {
        script.echo "Retrieving application version..."
        def packageJSON = script.readJSON file: 'package.json'
        def version = packageJSON.version
        def tag = "$version-$script.BUILD_NUMBER"
        return tag
    }

    def incrementVersion() {
        script.echo "Incrementing application version..."
        script.sh 'npm version patch'
    }

}