#!/usr/bin/env groovy
import com.example.Git

def call(String repo) {
    return new Git(this).gitCommitAndPush(repo)
}