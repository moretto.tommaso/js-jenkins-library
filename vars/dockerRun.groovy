#!/usr/bin/env groovy
import com.example.Docker

def call(String imageName, String dockerContainerName, String credential, String remoteUrl) {
    return new Docker(this).dockerRun(imageName, dockerContainerName, credential, remoteUrl)
}