#!/usr/bin/env groovy
import com.example.Docker

def call(String imageName, String credential, String remoteUrl) {
    return new Docker(this).dockerCompose(imageName, credential, remoteUrl)
}